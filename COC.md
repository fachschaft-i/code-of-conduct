# Code of Conduct, Fachschaft-I HSMA

## Warum brauchen wir einen Verhaltenskodex?

* Wir möchten, dass sich auf unseren Veranstaltungen jede:r sicher und willkommen fühlt. Insbesondere neue Besucher:innen oder Personen mit schlechten Erfahrungen in anderen Gruppen sollen sich bei uns wohlfühlen können. Wir möchten bereits im Vorfeld die Erwartung kommunizieren, dass Belästigungen und anderes unangenehmes Verhalten bei uns nicht akzeptabel sind.
* Wir möchten Vertrauen schaffen, dass wir Berichte über Fehlverhalten ernst nehmen, nicht die Schuld auf das Opfer schieben und jeden Vorfall genau untersuchen, selbst wenn der Vorfall eine Person betrifft, die schon lange bei uns ist oder sich sonstwie in einer herausgehobenen Position befindet.
* Wir möchten klarstellen, welches Verhalten wir in unserer Community akzeptabel finden. Nicht alle von uns haben den selben Hintergrund oder die gleichen Vorerfahrungen, und indem wir unsere Erwartungen explizit und transparent machen, bringen wir alle auf den selben Stand.
* Wir möchten einen definierten Prozess haben, wie wir mit unangenehmen Vorfällen umgehen. Dieser Verhaltenskodex schafft die Grundlage für das Vorgehen in solchen Fällen, sowie die Entscheidung, wann so ein Fall vorliegt.

## Unser Verhaltenskodex

* Sei freundlich, rücksichtsvoll  und respektvoll  im Umgang mit deinen Mitmenschen. Verzichte auf Beleidigungen oder Herabwürdigungen anderer Menschen in unserer Community.
* Wir möchten einen belästigungsfreien Rahmen für alle schaffen, unabhängig von zum Beispiel Herkunft, Ethnizität, Kultur, Nationalität, Hautfarbe, sozialer und wirtschaftlicher Stellung, Bildungsniveau, geschlechtlicher Ausprägung und Identifikation, sexueller Orientierung, Alter, Größe, Familienstatus, politischer Einstellung, Religion, sowie mentaler und physikalischer Leistungsfähigkeit.
* Wir tolerieren daher keinerlei Belästigung anderer Menschen in irgendeiner Form. Das schließt insbesondere verletzende oder herabwürdigende Kommentare bezüglich einer der vorgenannten Gruppen, bewusste Einschüchterung, Stalking, Verfolgung, unerwünschte Foto-, Video- oder Tonaufnahmen anderer Personen, wiederholte Störungen von Vorträgen oder anderen Veranstaltungen, gewaltvolle Sprache oder Drohungen, unerwünschten oder unangemessenen körperlichen Kontakt sowie unerwünschte sexuelle Annäherung ein. Wir finden sexualisierte oder gewaltverherrlichende Text- und Bildsprache auf unseren Veranstaltungen und Plattformen unangemessen.
* Sei achtsam in deiner Wortwahl. Bedenke, dass sexistische, rassistische und anderweitig gruppenbezogen ausschließende Witze beleidigend sein können und dazu führen können, dass sich andere Personen in deinem Umfeld unwohl fühlen. Wir finden daher, dass solche Witze für unsere Community unangemessen sind.
* Wer mit Ideen von Rassismus, Ausgrenzung und damit verbundener struktureller und körperlicher Gewalt auf uns zukommt, hat sich vom Dialog verabschiedet und ist jenseits der Akzeptanzgrenze. Wer es darauf anlegt, das Zusammenleben in dieser Gesellschaft zu zerstören und auf eine alternative Gesellschaft hinarbeitet, deren Grundsätze auf Chauvinismus und Nationalismus beruht, arbeitet gegen die moralischen Grundsätze, die uns verbinden. Wir erklären daher das Vertreten von Rassismus und von der Verharmlosung der historischen und aktuellen faschistischen Gewalt für unvereinbar mit einer Teilnahme an unseren Veranstaltungen und Plattformen.

## Wohin kann ich mich an Problemen wenden?

Wenn du in einen Vorfall verwickelt wirst oder bezeugen kannst, wende dich bitte schnellstmöglich an eine:n Representant:in der Fachschaft.

## Melden von Problemen

Bitte zögere nicht, dich zu melden, wenn du unsere Hilfe brauchst. Du brauchst dich nicht sorgen, uns damit zu belästigen, selbst wenn du dich mehrfach an uns wendest. Wir betrachten Meldungen als Möglichkeit, Informationen zu sammeln und bei Bedarf zu handeln: Nur wenn wir Bescheid wissen, können wir etwas tun und gegebenenfalls ähnliche Vorfälle in der Zukunft verhindern. Wenn du dir nicht sicher bist, ob eine Situation gegen den Verhaltenskodex verstößt, bitten wir dich, dich trotzdem an uns zu wenden.

Wenn du dich nicht wohl dabei fühlst, die Gespräche mit uns zu führen, kannst du gerne ein Vertrauensperson bestimmen, die sich mit dir oder für dich an uns wendet.

Wenn wir von einem Vorfall erfahren, folgen wir den unten aufgeführten Schritten.

### Handeln als Team

Fachschaftssprecher:innen und Code-of-Conduct beauftragte Personen handeln in diesen Fällen möglichst nie alleine. Selbst bei einfachen Aufgaben wie der Antwort auf eine E-Mail sollten möglichst immer zwei Personen involviert sein.

Wenn möglich, soll daher jeder Vorfall vor Ergreifen einer Handlung immer mindestens mit einem anderen Fachschaftsmitglied besprochen werden.

Jede Meldung und darauf basierende Entscheidung wird schriftlich festgehalten. Selbst wenn das zum aktuellen Zeitpunkt überflüssig erscheinen mag, ist es zu leicht, wichtige Details (insbesondere in der Hektik einer Veranstaltung) schnell zu vergessen.

### Interessenskonflikte

So schnell wie möglich, aber spätestens bei der Beratung des Vorfalls, müssen Fachschaftsmitglieder mögliche Interessenskonflikte aufzeigen. Hierzu zählen Freundschaften mit einer der involvierten Personen oder alles andere, was eine Neutralität behindert.

Wenn möglich, sollen Gespräche mit den beteiligten Personen nicht von deren Freunden geführt werden, da dies für alle Beteiligten sehr unangenehm sein kann.

Wenn ein Fachschaftsmitglied über einen Vorfall informiert wird, in den ein anderes Fachschaftsmitglied verwickelt ist, wird das das betroffene Fachschaftsmitglied selbstverständlich nicht in die interne Kommunikation zu dem Vorfall einbezogen, sondern wie eine normale Student:in behandelt.

### Findung von Maßnahmen

Oberstes Ziel ist es, den konkreten Vorfall angemessen und fair aufzulösen, nicht eine Bestrafung handelnder Personen. Uns ist bewusst, dass eine abschließende Wahrheitsfindung und lückenlose Beweisführung in vielen Fällen nicht mit vertretbarem Aufwand möglich sein wird. In diesen Fällen hat die Reduktion des Risikos ähnlicher Vorfälle in der Zukunft und die Wiederherstellung einer Atmosphäre, in der sich Betroffene und Mitglieder diskriminierter Gruppen wohl und sicher fühlen, Vorrang.

Beispielsweise ist es daher relevant, ob ein Vorfall absichtlich herbeigeführt wurde oder nicht, insbesondere in leichten Fällen wie z.B. unangemessener Witze. In absichtlichen Fällen oder besonders schweren Fällen werden wir uns eher für gewichtigere Reaktionen entscheiden.

### Information der Betroffenen

Die Person, die den Vorfall gemeldet hat, wird über jegliche getroffenen Maßnahmen und die zugrundeliegenden Gründe informiert.

### Öffentliche Aussagen

Grundsätzlich äußert sich die Fachschaft nicht öffentlich zu Vorfällen, die einzelne Personen involvieren.

---

Dieser Verhaltenskodex basiert zum großen Teil auf Texten der [Django-Community](https://2018.djangocon.eu/conduct/) und des [NoName e.V.](https://noname-ev.de), wofür wir uns bedanken möchten.
